import java.util.LinkedList;

public class Person {
	public int id;
	String nume;
	
	
	
	public Person(int id, String nume)
	{
		this.id = id;
		this.nume = nume;
		
	}
	

	
	
	public String getNume() {
		return nume;
	}
	
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

}
