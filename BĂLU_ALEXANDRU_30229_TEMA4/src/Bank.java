import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

@SuppressWarnings("serial")
public class Bank implements BankProc{
	HashMap<Integer,Account> conturi;
	HashMap<Integer,Person> persoane;
	
	public Bank()
	{
		conturi = new HashMap<Integer,Account>();
		persoane = new HashMap<Integer,Person>();
		//this.deserialize();
	}
	
	
	//add edit delete person
	public void addAccount(Account a)
	{
		assert(a!=null);
		conturi.put(a.getId(), a);
		this.serialize();
	}
	
	public void editAccount(Account p)
	{
		assert(p!=null);
		conturi.remove(p.getId());
		conturi.put(p.getId(),p);
		this.serialize();
	}
	
	public void deleteAccount(int id)
	{
		assert(id>0);
		conturi.remove(id);
		this.serialize();
	}
	
	
	
	//add/edit/delete person
	public void addPerson(Person p)
	{
		assert(p!=null);
		persoane.put(p.getId(), p);
		this.serialize();
	}
	
	
	
	public void editPerson(Person p)
	{
		assert(p!=null);
		persoane.remove(p.getId());
		persoane.put(p.getId(),p);
		this.serialize();
	}
	
	public void deletePerson(int id)
	{
		assert(id>0);
		persoane.remove(id);
		this.serialize();
	}
	
	public HashMap<Integer, Account> getConturi() {
		return conturi;
	}
	
	public HashMap<Integer, Person> getPersoane() {
		return persoane;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deserialize() {
		try {
	         FileInputStream fisierIntrare = new FileInputStream("conturi.ser");
	         ObjectInputStream in = new ObjectInputStream(fisierIntrare);
	         conturi = (HashMap<Integer,Account>)in.readObject();
	         in.close();
	         fisierIntrare.close();
	         FileInputStream fisierIntrare2 = new FileInputStream("persoane.ser");
	         ObjectInputStream in2 = new ObjectInputStream(fisierIntrare2);
	         persoane = (HashMap<Integer,Person>)in.readObject();
	         in.close();
	         fisierIntrare2.close();
		}catch(IOException i) {
			i.printStackTrace();
		}catch(ClassNotFoundException c) {
			c.printStackTrace();
		}	
	}
	
	public void serialize() {
		try {
	         FileOutputStream fisierIesire = new FileOutputStream("conturi.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fisierIesire);
	         out.writeObject(conturi);
	         out.close();
	         fisierIesire.close();
	         
	         FileOutputStream fisierIesire2 = new FileOutputStream("persoane.ser");
	         ObjectOutputStream out2 = new ObjectOutputStream(fisierIesire2);
	         out.writeObject(persoane);
	         out.close();
	         fisierIesire2.close();
	         System.out.println("Datele s-au salvat in fisierele conturi.ser si persoane.ser");
		}catch(IOException i) {
	         i.printStackTrace();
	    }		
	}
	
}
