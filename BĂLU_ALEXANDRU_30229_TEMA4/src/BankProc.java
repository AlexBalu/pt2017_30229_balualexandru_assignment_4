import java.io.Serializable;

public interface BankProc extends Serializable {

	
	/*
	 * adauga cont
	 * @pre a!=NULL 
	 */
	public void addAccount(Account a);
	
	/*
	 * edit cont
	 * @pre p!=NULL 
	 */
	public void editAccount(Account p);
	
	/*
	 * stergere cont
	 * @pre id > 0
	 */
	public void deleteAccount(int id);
		

	/*
	 * adauga persoana
	 * @pre a!=NULL
	 */	
	public void addPerson(Person p);
	
	
	/*
	 * editeaza persoana
	 * @pre p!=NULL
	 */	
	public void editPerson(Person p);
	
	/*
	 * editeaza persoana
	 * @pre id>0
	 */	
	public void deletePerson(int id);
		
		
	
}
