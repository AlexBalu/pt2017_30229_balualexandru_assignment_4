

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JTable;

public class Reflection {
	public static JTable createJTable(HashMap<Integer,Object> hash,Object[] atribute) {
		Object [][] v = new Object[hash.size()][atribute.length];
		int i=0;
		int j=0;
		
	    Set set = hash.entrySet();
        Iterator it = set.iterator();
	
		 while(it.hasNext()) {
			for (Field field : it.getClass().getDeclaredFields()) {
				field.setAccessible(true); 
				Object obj;
				
				try {
					obj = field.get(it);
					v[i][j]=obj;
					j++;
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}			
			}
			i++;
			j=0;
		}
		
		JTable t = new JTable(v,atribute);
		return t;
	}
	
	
	
}
