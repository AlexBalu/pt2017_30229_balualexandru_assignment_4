import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class OpPersoane {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private Bank b;


	/**
	 * Create the application.
	 */
	public OpPersoane(Bank b) {
		initialize();
		this.b = b;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		JLabel lblAdaugaPersoana = new JLabel("Adauga Persoana");
		lblAdaugaPersoana.setBounds(10, 11, 92, 14);
		frame.getContentPane().add(lblAdaugaPersoana);
		
		JLabel lblId = new JLabel("id");
		lblId.setBounds(10, 31, 46, 14);
		frame.getContentPane().add(lblId);
		
		textField = new JTextField();
		textField.setBounds(36, 28, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNume = new JLabel("nume");
		lblNume.setBounds(10, 60, 46, 14);
		frame.getContentPane().add(lblNume);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(36, 57, 86, 20);
		frame.getContentPane().add(textField_1);
		
		JButton btnAddPerson = new JButton("AddPerson");
		btnAddPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person p = new Person(Integer.parseInt(textField.getText()),textField_1.getText());
				b.addPerson(p);
			}
		});
		btnAddPerson.setBounds(20, 88, 89, 23);
		frame.getContentPane().add(btnAddPerson);
		
		JLabel lblEditPerson = new JLabel("Edit Person");
		lblEditPerson.setBounds(132, 11, 92, 14);
		frame.getContentPane().add(lblEditPerson);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(158, 28, 86, 20);
		frame.getContentPane().add(textField_2);
		
		JLabel label_1 = new JLabel("id");
		label_1.setBounds(132, 31, 46, 14);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("nume");
		label_2.setBounds(132, 60, 46, 14);
		frame.getContentPane().add(label_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(158, 57, 86, 20);
		frame.getContentPane().add(textField_3);
		
		JButton btnEditperson = new JButton("EditPerson");
		btnEditperson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person p = new Person(Integer.parseInt(textField_2.getText()),textField_3.getText());
				b.addPerson(p);
			}
		});
		btnEditperson.setBounds(142, 88, 89, 23);
		frame.getContentPane().add(btnEditperson);
		
		JLabel lblDeleteperson = new JLabel("DeletePerson");
		lblDeleteperson.setBounds(265, 11, 92, 14);
		frame.getContentPane().add(lblDeleteperson);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(291, 28, 86, 20);
		frame.getContentPane().add(textField_4);
		
		JLabel label_3 = new JLabel("id");
		label_3.setBounds(265, 31, 46, 14);
		frame.getContentPane().add(label_3);
		
		JButton btnDeleteperson = new JButton("DeletePerson");
		btnDeleteperson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b.deletePerson(Integer.parseInt(textField_4.getText()));
			}
		});
		btnDeleteperson.setBounds(275, 88, 89, 23);
		frame.getContentPane().add(btnDeleteperson);
	}
}
