import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class OpAccount {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	Bank b;
	private JTextField textField_7;
	private JTextField textField_8;


	/**
	 * Create the application.
	 */
	public OpAccount(Bank b) {
		initialize();
		this.b = b;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("SavingAccount");
		rdbtnNewRadioButton.setBounds(52, 184, 109, 23);
		frame.getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnSpendingaccount = new JRadioButton("SpendingAccount");
		rdbtnSpendingaccount.setBounds(52, 219, 109, 23);
		frame.getContentPane().add(rdbtnSpendingaccount);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(36, 57, 86, 20);
		frame.getContentPane().add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(36, 28, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(36, 80, 86, 20);
		frame.getContentPane().add(textField_2);
		
		JButton btnAddaccount = new JButton("AddAccount");
		btnAddaccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Account c;
				if(rdbtnNewRadioButton.isSelected() == true)
				 c = new SavingAccount(Integer.parseInt(textField_1.getText()),Integer.parseInt(textField.getText()),Integer.parseInt(textField_2.getText()),Integer.parseInt(textField_7.getText()));
			else
				 c = new SavingAccount(Integer.parseInt(textField_3.getText()),Integer.parseInt(textField_4.getText()),Integer.parseInt(textField_5.getText()),Integer.parseInt(textField_8.getText()));
			b.addAccount(c);
			}
		});
		btnAddaccount.setBounds(13, 154, 109, 23);
		frame.getContentPane().add(btnAddaccount);
		
		JLabel lblTitular = new JLabel("titular");
		lblTitular.setBounds(10, 60, 46, 14);
		frame.getContentPane().add(lblTitular);
		
		
		
		JLabel label_1 = new JLabel("id");
		label_1.setBounds(10, 31, 46, 14);
		frame.getContentPane().add(label_1);
		
		JLabel lblAdaugacont = new JLabel("AdaugaCont");
		lblAdaugacont.setBounds(10, 11, 92, 14);
		frame.getContentPane().add(lblAdaugacont);
		
		JLabel lblSuma = new JLabel("suma");
		lblSuma.setBounds(10, 83, 46, 14);
		frame.getContentPane().add(lblSuma);
		
		
		
		JLabel lblEditcont = new JLabel("EditCont");
		lblEditcont.setBounds(132, 11, 92, 14);
		frame.getContentPane().add(lblEditcont);
		
		JLabel label_2 = new JLabel("id");
		label_2.setBounds(132, 31, 46, 14);
		frame.getContentPane().add(label_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(158, 28, 86, 20);
		frame.getContentPane().add(textField_3);
		
		JLabel label_3 = new JLabel("titular");
		label_3.setBounds(132, 60, 46, 14);
		frame.getContentPane().add(label_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(158, 57, 86, 20);
		frame.getContentPane().add(textField_4);
		
		JLabel label_4 = new JLabel("suma");
		label_4.setBounds(132, 83, 46, 14);
		frame.getContentPane().add(label_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(158, 80, 86, 20);
		frame.getContentPane().add(textField_5);
		
		JButton btnEditaccount = new JButton("EditAccount");
		btnEditaccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEditaccount.setBounds(135, 154, 89, 23);
		frame.getContentPane().add(btnEditaccount);
		
		JButton btnDeleteaccount = new JButton("DeleteAccount");
		btnDeleteaccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteaccount.setBounds(260, 154, 109, 23);
		frame.getContentPane().add(btnDeleteaccount);
		
		JLabel label = new JLabel("id");
		label.setBounds(257, 31, 46, 14);
		frame.getContentPane().add(label);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(283, 28, 86, 20);
		frame.getContentPane().add(textField_6);
		
		JLabel lblDeletecont = new JLabel("DeleteCont");
		lblDeletecont.setBounds(257, 11, 92, 14);
		frame.getContentPane().add(lblDeletecont);
		
		JLabel lblDobanda = new JLabel("dobanda");
		lblDobanda.setBounds(10, 114, 46, 14);
		frame.getContentPane().add(lblDobanda);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(36, 111, 86, 20);
		frame.getContentPane().add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(158, 108, 86, 20);
		frame.getContentPane().add(textField_8);
		
		JLabel label_5 = new JLabel("dobanda");
		label_5.setBounds(132, 111, 46, 14);
		frame.getContentPane().add(label_5);
		
		
	}
}
