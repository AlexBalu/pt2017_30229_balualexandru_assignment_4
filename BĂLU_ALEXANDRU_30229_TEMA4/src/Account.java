
public abstract class Account {
protected int id;
protected int titular;
protected int suma;

public Account(int id,int suma,int titular)
{
	this.id = id;
	this.suma = suma;
	this.titular = titular;
}

public abstract void retrageBani(int suma);

public abstract void depuneBani(int suma);


public int getId() {
	return id;
}

public int getTitular() {
	return titular;
}
public void setTitular(int titular) {
	this.titular = titular;
}

}
