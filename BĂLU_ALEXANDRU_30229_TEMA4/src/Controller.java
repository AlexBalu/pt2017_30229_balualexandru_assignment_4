import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Controller extends JFrame {
Bank bank;
private JScrollPane s ;
private JPanel p;

public Controller(Bank b)
{
	this.bank = b;
	p = new JPanel();
	this.setVisible(true);
}

public void viewAccounts()
{
	HashMap<Integer,Account> hmap = bank.getConturi();
	HashMap<Integer,Object> hmapOb = new HashMap<Integer,Object>();
	
	Set set = hmap.entrySet();
    Iterator it = set.iterator();
    while(it.hasNext())
    {
    	Map.Entry mentry = (Map.Entry)it.next();
    	hmapOb.put((Integer)mentry.getKey(), mentry.getKey());
    }
	Object[] atribute = { "id_cont","suma","id_titular","dobanda"};
	
	s = new JScrollPane(Reflection.createJTable(hmapOb,atribute));
	p.add(s);
	this.add(p,BorderLayout.CENTER);
	this.setVisible(true);
	this.setSize(600, 600);

}


public void viewPersons()
{
	
	HashMap<Integer,Person> hmap = bank.getPersoane();
	HashMap<Integer,Object> hmapOb = new HashMap<Integer,Object>();
	
	Set set = hmap.entrySet();
    Iterator it = set.iterator();
    while(it.hasNext())
    {
    	Map.Entry mentry = (Map.Entry)it.next();
    	hmapOb.put((Integer)mentry.getKey(), mentry.getKey());
    }
	Object[] atribute = { "id_persoana","nume"};
	
	s = new JScrollPane(Reflection.createJTable(hmapOb,atribute));
	p.add(s);
	this.add(p,BorderLayout.CENTER);
	this.setVisible(true);
	this.setSize(600, 600);
	
}


}

