import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class View extends JFrame {

	private JFrame frame;
	private Bank theBank;

	/**
	 * Create the application.
	 */
	public View(Bank bank) {
		theBank = bank;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		JButton btnOperatiiConturi = new JButton("Operatii Conturi");
		btnOperatiiConturi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpAccount oPersView = new OpAccount(theBank);
			}
		});
		btnOperatiiConturi.setBounds(129, 30, 169, 55);
		frame.getContentPane().add(btnOperatiiConturi);
		
		JButton button = new JButton("Operatii Persoane");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpPersoane oPersView = new OpPersoane(theBank);
			}
		});
		button.setBounds(129, 122, 169, 55);
		frame.getContentPane().add(button);
	}
}
